<?php
include_once("konf.php");
include_once("mycdb.php");
$cDB = new cDatabase($Db["db_host"], $Db["db_user"], $Db["db_password"], $Db["db_name"] , false);
$pageName="info";
if (!isset($_GET["idtrck"])) {
	exit;
}
function html_icon($name,$url,$tekst){
	$ikoon=$GLOBALS['iconArray'][$name];
	$Icpos=0*$GLOBALS['iconWidth'];
	if ($tekst==""){
		$tekst==$ikoon[1];
	}
	$Icpos=$ikoon[0]*$GLOBALS['iconWidth'];
	if ($url==""){
		$url="#";
	}
	echo "<a href=\"{$url}\" targe=\"outUrl\" title=\"{$tekst}\"><div class=\"etricon\" style=\"background-position: 0 -{$Icpos}px\" /></a>";
}

$trackId=(int)$_GET["idtrck"];
$sql="SELECT * FROM {$Env['table']}maininfo WHERE id={$trackId}";
$rs=$cDB->ExecuteReader($sql);
if ($trackData = @mysql_fetch_array($rs, MYSQL_ASSOC)) {
?>
<!DOCTYPE html>
<html>
<head>
<title>ETR</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="Description" content="Eesti Terviserajad">
<meta name="Keywords" content="Terviserajad, jne">
<link href="gfx/styles.css" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="gfx/jqx.base.css" type="text/css" />
<script src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0" type="text/javascript"></script>
<script type="text/javascript" src="js/etr.js"></script>
<script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="js/jqxcore.js"></script>
<script type="text/javascript" src="js/jqxdata.js"></script>
<script type="text/javascript" src="js/jqxchart.js"></script>
<script type="text/javascript">
<?php echo "var pinLoc=new Microsoft.Maps.Location(".$trackData['geo_pos'].");"; ?>
function GetETRMap()
{
	// Initialize the map
	Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', { callback: function () {
	map = new Microsoft.Maps.Map(document.getElementById("etrMap"),
	 {credentials:'<?php echo $BingMapKey;?>',
	 showScalebar: false,
	 showMapTypeSelector: false,
	 enableSearchLogo: false,
	 theme: new Microsoft.Maps.Themes.BingTheme()});
	map.setView({ zoom: 12,
	center: pinLoc,
	mapTypeId: Microsoft.Maps.MapTypeId.automatic });
	map.entities.clear();
	var pushpin = new Microsoft.Maps.Pushpin(pinLoc, {icon: 'gfx/etrflag.png', width: 46, height: 15, typeName: 'pinstyle', text : '', visible: true});
	map.entities.push(pushpin);
	}
  });
}
</script>
</head>
<body id="main_body" onLoad="GetETRMap()">
<div id="form_container">
<h1><?php echo $trackData['name']; ?></h1>
<table>
<tr><td>Maakond:</td><td><?php echo $trackData['county']; ?></td></tr>
<?php
if ($trackData['description']!=""){
	echo "<tr><td colspan=\"2\">".$trackData['description']."</td></tr>";
}
if ($trackData['address']!=""){
	echo "<tr><td>Aadress:</td><td>".$trackData['address']."</td></tr>";
}
if ($trackData['opentime']!=""){
	echo "<tr><td>Avatud:</td><td>".$trackData['opentime']."</td></tr>";
}
if ($trackData['contact']!=""){
	echo "<tr><td>Kontakt:</td><td>".$trackData['contact']."</td></tr>";
}
if ($trackData['link_email']!=""){
	echo "<tr><td>e-post:</td><td>".str_replace("@","((a))",$trackData['link_email'])."</td></tr>";
}
if ($trackData['link_web']!=""){
	echo "<tr><td>e-post:</td><td><a href=\"".$trackData['link_web']."\" target=\"outURL\">".$trackData['link_web']."</td></tr>";
}
if ($trackData['link_ilmee']!=""){
	echo "<tr><td>Kohalik ilm:</td><td><a href=\"".$trackData['link_ilmee']."\" target=\"outURLIlm\">".$trackData['link_ilmee']."</td></tr>";
}
if ($trackData['link_lumeinfo']!=""){
	echo "<tr><td>Lumeinfo:</td><td><a href=\"".$trackData['link_lumeinfo']."\" target=\"outURLLumi\">".$trackData['link_lumeinfo']."</td></tr>";
}
?>
	<tr><td colspan="2"><table><tr>
<?php
	$sql="SELECT * FROM {$Env['table']}mainicons WHERE main_id={$trackId} ORDER BY groupline";
	$rs2=$cDB->ExecuteReader($sql);
	$rida=1;
	while ($row2 = @mysql_fetch_array($rs2, MYSQL_ASSOC)) {
	if ($rida!=$row2['groupline']) {
		echo "</tr><tr>";
		$rida=$row2['groupline'];
		}
		echo "<td>";
		echo html_icon($row2['typename'],$row2['url'],$row2['descript']);
		echo "</td>";
	}
?>
	</tr></table></td></tr>
</table>
<h3>Kaart ja rajad</h3>
<table>
	<tr>
	<td valign="top">
	<div id="etrMap"></div>
    <div id='jqxProfiil' style="width:400px; height:150px"></div>
	</td><td valign="top"><ul>
<?php
	$sql="SELECT id, track_name, colour, track_data FROM {$Env['table']}track WHERE main_id={$trackId} ORDER BY track_name";
	$rs3=$cDB->ExecuteReader($sql);
	while ($row3 = @mysql_fetch_array($rs3, MYSQL_ASSOC)) {
		echo "<li><div onClick=\"ShowTrack(".$row3['id'].")\" class=\"trckname\" style=\"border-bottom-color: ".$row3['colour']."\">".$row3['track_name']."</div>";
		$sql="SELECT typename FROM {$Env['table']}trackicons WHERE track_id={$row3['id']} ORDER BY typename";
		$rs4=$cDB->ExecuteReader($sql);
		echo "<table><tr>";
			while ($row4 = @mysql_fetch_array($rs4, MYSQL_ASSOC)) {
				echo "<td>";
				echo html_icon($row4['typename'],"","");
				echo "</td>";
			}
		echo "</tr></table></li>";
		echo "<script type=\"text/javascript\">trck[".$row3['id']."]=new Array(".$row3['track_data']."); color[".$row3['id']."]='".$row3['colour']."';</script>";
	}
?>
	<ul>
	</td>
	</tr>
</table>

<?php settype($rs, "null"); settype($cDB, "null");?>
<div id="footer"> Eesti Terviserajad 2013</div>
</div>
</body>

</html>
<?php } ?>