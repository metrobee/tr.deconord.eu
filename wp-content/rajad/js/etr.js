var map = null;
var color = [];
var trck = [];
var pushpinCenter = { icon: 'gfx/etrflag.png', width:46, height:15};
var pushpinStart = { icon: 'gfx/start.png', width:38, height:14};
var pushpinKMstyle = { icon: 'gfx/etrflag.png', width:15, height:15};
var posCenter = null;
var posElementDoc = null;

function confDelete() {
	var answer = confirm("Soovid kustutada?");
	if (answer){
		return true;
	}
	else{
		return false;
	}
}
function changeVisible(elem_id)
{
	if (document.getElementById(elem_id).style.display=='none') {
		document.getElementById(elem_id).style.display='block';
	} else {document.getElementById(elem_id).style.display='none';}
}

function ShowLocation(mapPos)
{
	if (map!==null){
		var n=mapPos.split(",");
		map.setView({ zoom: 11, center: new Microsoft.Maps.Location(n[0],n[1]) });
	}
}

function ShowInfo(id)
{
	TINY.box.show({iframe:"<?php echo WP_CONTENT_DIR; ?> + rada.php?idtrck=+id,width:680,height:650"});
}
//{'a':59.31580103,'o':24.3910064735,'e':45.8,'d':0}
function ShowTrack(id)
{
	map.entities.clear();
	var pushpin = new Microsoft.Maps.Pushpin(pinLoc, {icon: 'gfx/etrflag.png', width: 46, height: 15, typeName: 'pinstyle', text : '', visible: true});
	map.entities.push(pushpin);
	if (trck[id]!==null){
		var objStart = trck[id][0];
		var linestyle = { strokeColor: new Microsoft.Maps.Color.fromHex(color[id]), strokeThickness: 3};
		var pushpin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(objStart.a, objStart.o), pushpinStart);
		map.entities.push(pushpin);
		var polArray = [];
		var kmPost = 1000;
		var maxH=0; var minH=999;
		for(var i=0; i<trck[id].length; i++) {
			var objPoint = trck[id][i];
			var polPoint = new Microsoft.Maps.Location(objPoint.a, objPoint.o);
            polArray.push(polPoint);
			if (objPoint.d>kmPost) {
				pushpinKMstyle.text=(kmPost/1000).toFixed(0)+' km';
				var pushpinKM = new Microsoft.Maps.Pushpin(polPoint, pushpinKMstyle);
				map.entities.push(pushpinKM);
				//var infopin = new Microsoft.Maps.Infobox(polPoint, { title: (kmPost/1000).toFixed(0)+' km', description: 'Kõrgus:'+objPoint.e+' m', pushpin: pushpinKM});
				//map.entities.push(infopin);
				kmPost=kmPost+1000;
			}
			if (objPoint.e>maxH) { maxH=objPoint.e;}
			if (objPoint.e<minH) { minH=objPoint.e;}
		}
		var polyline = new Microsoft.Maps.Polyline(polArray, linestyle);
		map.entities.push(polyline);
		map.setView({ zoom: 14, center: new Microsoft.Maps.Location(objStart.a, objStart.o) });

		var dataS =
		{
			datatype: "json",
			datafields: [
				{ name: 'a' },
				{ name: 'o' },
				{ name: 'e' },
				{ name: 'd' }
			],
			localdata: trck[id]
		};
		var dataAdapter = new $.jqx.dataAdapter(dataS, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });
		// prepare jqxChart settings
		var settings = {
			title: "Raja profiil",
			description: "",
			enableAnimations: true,
			showLegend: false,
			padding: { left:10, top: 5, right: 10, bottom: 5 },
			titlePadding: { left: 30, top: 0, right: 0, bottom: 4 },
			source: dataAdapter,
			categoryAxis:
				{
					dataField: 'd',
					formatFunction: function (value) {
						var num=(value/1000);
						return num.toFixed(1);
					},
					toolTipFormatFunction: function (value) {
						var num=(value/1000);
						return num.toFixed(1)+ 'km';
					},
					type: 'default',
					showTickMarks: true,
					unitInterval: 100,
					showGridLines: true,
					gridLinesInterval: 10000,
					gridLinesColor: '#888888',
					valuesOnTicks: true
				},
			colorScheme: 'scheme04',
			seriesGroups:
				[
					{
						type: 'line',
						valueAxis:
						{
							unitInterval: 10,
							minValue: minH,
							maxValue: maxH,
							displayValueAxis: true,
							description: 'Kõrgus',
							axisSize: 'auto',
							tickMarksColor: '#888888'
						},
						series: [
						{ dataField: 'e', displayText: 'Kõrgus', color : color[id] }
						]
					}
				]
		};
		// profiil:
		$('#jqxProfiil').jqxChart(settings);
	}

}
function GetBingMap(mapAreaName, posElement) {
	posElementDoc = document.getElementById(posElement);
	var pos= posElementDoc.value.toString();
	if (pos !=="") {
		var posLL = pos.split(",", 2);
		posCenter = new Microsoft.Maps.Location(posLL[0], posLL[1]);
	} else {
		posCenter = new Microsoft.Maps.Location(58.5970, 24.9280);
	}
	map = new Microsoft.Maps.Map(document.getElementById(mapAreaName),
		{ credentials: "AuGNZkwk4TnA-WEMtC7KWKaeEvgQc2JvNj7xRxx2XmU8TNlFg8WJTDVBdNoQwSRM",
			showScalebar: true,
			showDashboard: true,
			enableSearchLogo: false,
			mapTypeId: Microsoft.Maps.MapTypeId.road,
			zoom: 5,
			center: posCenter
		});
		Microsoft.Maps.Events.addHandler(map, 'click', getLatLong);

	setPin();
}
function getLatLong(e) {
	if (e.targetType == "map") {
		var point = new Microsoft.Maps.Point(e.getX(), e.getY());
		var loc = e.target.tryPixelToLocation(point);
		var sLat = loc.latitude.toString();
		var sLon = loc.longitude.toString();
		if (posElementDoc!==null){
			posElementDoc.value = sLat.substring(0,8)+','+sLon.substring(0,8);
			posCenter = loc;
			setPin();
		}
	}
}
function setPin() {
	var pushPin = new Microsoft.Maps.Pushpin(posCenter,
	{ text: 'Keskus', visible: true });
	map.entities.clear();
	map.entities.push(pushPin);
	map.setView({ center: posCenter });
}