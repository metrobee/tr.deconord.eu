<?php
//File written by Ryan Campbell
//June 2005 - Particletree / 2007 Tarmo Klaar
// http://particletree.com/features/database-simplicity-with-class/
//Class to handle database operations
class cDatabase {

  //class variables defined in constructor
  var $host;
  var $user;
  var $password;
  var $databaseName;
  var $debug;
  
  //constructor - needed for connection string
  function cDatabase($hostName, $userName, $passwordName, $databaseName, $deBug=false){
    $this->host = $hostName;
    $this->user = $userName;
    $this->password = $passwordName;
    $this->database = $databaseName;
    $this->debug = $deBug;
  }

  //loop through paired arrays buildng an sql INSERT statement
  function sqlInsert($dataNames, $dataValues, $tableName){
	  $sqlNames = "INSERT INTO " . $tableName . "(";
	  $sqlValues ="";
	  for($x = 0; $x < count($dataNames); $x++) {
		  if($x != (count($dataNames) - 1)) {
			  $sqlNames = $sqlNames . $dataNames[$x] . ", ";
			  $sqlValues = $sqlValues . "'" . addslashes($dataValues[$x]) . "', ";
		  }
		  else {
			  $sqlNames = $sqlNames . $dataNames[$x] . ") VALUES(";
			  $sqlValues = $sqlValues . "'" . addslashes($dataValues[$x]) . "')";
		  }
	  }
	  if ($this -> debug == true) { echo $sqlNames . $sqlValues; }
	  // $this->ExecuteNonQuery($sqlNames . $sqlValues);
	$conn = @mysql_connect($this->host, $this->user, $this->password);
    mysql_select_db ($this->database);
	$rs = mysql_query($sqlNames . $sqlValues,$conn);
	$id = mysql_insert_id();
    settype($rs, "null");
	mysql_close($conn);
	return $id;
	  
  }

  //loop through paired arrays buildng an sql UPDATE statement
  function sqlUpdate($dataNames, $dataValues, $tableName, $condition){
	  $sql = "UPDATE " . $tableName . " SET ";
	  for($x = 0; $x < count($dataNames); $x++) {
		  if($x != (count($dataNames) - 1)) {
			  $sql = $sql . $dataNames[$x] . "= '" . addslashes($dataValues[$x]) . "', ";
		  }
		  else {
			  $sql = $sql . $dataNames[$x] . "= '" . addslashes($dataValues[$x]) . "' ";
		  }
	  }
	  $sql = $sql . $condition;
	  if ($this -> debug == true) {  echo $sql; }
    $this->ExecuteNonQuery($sql);
  }
  
  //update password
  function sqlUpdatePassword($passFieldName, $newPass, $tableName, $passChangeField, $condition){
	  $sql = "UPDATE " . $tableName . " SET ";
	  $sql = $sql . $passFieldName . "= PASSWORD('" . $newPass . "'), ";
	  $sql = $sql . $passChangeField. "= NOW() ";	  
	  $sql = $sql ." WHERE ". $condition;
	  if ($this -> debug == true) {  echo $sql; }
    $this->ExecuteNonQuery($sql);
  }  

  //execute a query
  function ExecuteNonQuery($sql){
	$conn = @mysql_connect($this->host, $this->user, $this->password);
	if ($this -> debug == true) { echo $sql; }
    mysql_select_db ($this->database);
	$rs = mysql_query($sql,$conn);
    settype($rs, "null");
	mysql_close($conn);
  }
  
  //execute a query and return a recordset
  function ExecuteReader($query){
    $conn = @mysql_connect($this->host, $this->user, $this->password);
	if ($conn) {
    if ($this -> debug == true) {  echo $query; }
	@mysql_select_db ($this->database);
	  $rs = @mysql_query($query,$conn);
    @mysql_close($conn);
    return $rs;
	}
  } 
}
?>