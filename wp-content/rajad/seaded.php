<?php 
$pageError="";
include_once("konf.php");
include_once("mycdb.php");
include_once("funks.php");
include_once("ui_html.php");
$pageName="seaded";
?>
<!DOCTYPE html>
<html>
<head>
<title>ETR</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<link href="gfx/styles.css" media="all" rel="stylesheet" type="text/css" />
<script charset="UTF-8" type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
<script type="text/javascript" src="js/etr.js"></script>
</head>

<body id="main_body">
<div id="form_container">
<h1><a>ETR rajad</a></h1>
<?php 
if ($pageError!=""){
	echo "<div id=\"error_message\" class=\"pageError\">".$pageError."</div>";
}
$pContent= new cPage("PageView"); 
$pContent->env=$Env; 
$pContent->showContent($cDB,$pageName); 
settype($rs, "null"); settype($cDB, "null");?>
<div id="footer"> Eesti Terviserajad 2013</div>
</div>
</body>

</html>
