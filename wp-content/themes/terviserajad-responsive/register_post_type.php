<?php
Class EtrCustomPostTypes {

	public function __construct(){
		$this->register_tervisekeskused_cpt();
		$this->register_terviserajad_cpt();
		$this->tervisekeskus_amenities();
		$this->terviserada_amenities();
	}

	public function register_tervisekeskused_cpt(){
		$args = array(
		'labels' => array(
			'name' 					=> 'Tervisekeskused',
			'singular_name' 		=> 'Tervisekeskused',
			'add_new' 				=> "Lisa Uus",
			'add_new_item' 			=> "Lisa Uus Tervisekeskus",
			'edit_item' 			=> "Muuda Tervisekeskust",
			'new_item' 				=> 'Lisa kirje',
			'view_item' 			=> "Vaata Tervisekeskust",
			'search_items' 			=> "Otsi Tervisekeskust",
			'not_found' 			=> 'Ühtegi tervisekeskust ei ole sisestatud',
			'not_found_in_trash' 	=> 'Ei leidnud ühtegi tervisekeskust kustutatud asjade hulgast'
			),
		'query_var' => 'tervisekeskus',
		'rewrite' 	=> array(
			'slug'	=> 'tervisekeskus'),
		'public'	=> true,
		'has_archive' => true,
		'menu_icon' => get_stylesheet_directory_uri() .'/images/terviserajad-e.png',
		'supports'  => array('title', 'editor', 'thumbnail')
		);

		register_post_type('tervisekeskus', $args);
	}

	public function register_terviserajad_cpt(){
		$args = array(
		'labels' => array(
			'name' 					=> 'Terviserajad',
			'singular_name' 		=> 'Terviserajad',
			'add_new' 				=> "Lisa Uus",
			'add_new_item' 			=> "Lisa Uus Terviserada",
			'edit_item' 			=> "Muuda Terviserada",
			'new_item' 				=> 'Lisa kirje',
			'view_item' 			=> "Vaata Terviserada",
			'search_items' 			=> "Otsi Terviserada",
			'not_found' 			=> 'Ühtegi terviserada ei ole sisestatud',
			'not_found_in_trash' 	=> 'Ei leidnud ühtegi terviserada kustutatud asjade hulgast'
			),
		'query_var' => 'terviserada',
		'rewrite' 	=> array(
			'slug'	=> 'terviserada'),
		'public'	=> true,
		'has_archive' => true,
		'menu_icon' => get_stylesheet_directory_uri() .'/images/terviserajad-r.png',
		'supports'  => array('title', 'editor', 'thumbnail')
		);

		register_post_type('terviserada', $args);
	}

	public function tervisekeskus_amenities(){
		 $labels = array(
			'name'              => 'võimalused',
			'singular_name'     => 'võimalus',
			'search_items'      => 'otsi võimaluste hulgast',
			'all_items'         => 'kõik võimalused',
			'edit_item'         => 'muuda võimalust',
			'update_item'       => 'värskenda muutus',
			'add_new_item'      => 'lisa uus võimalus',
			'new_item_name'     => 'uue võimaluse nimi',
			'menu_name'         => 'Võimalused'
		  );
		register_taxonomy('võimalused', array('tervisekeskus'), array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'v6imalus' ),
		));
	}

		public function terviserada_amenities(){
		 $labels = array(
			'name'              => 'valmidused',
			'singular_name'     => 'valmidus',
			'search_items'      => 'otsi valmiduste hulgast',
			'all_items'         => 'kõik valmidused',
			'edit_item'         => 'muuda valmidus',
			'update_item'       => 'värskenda valmidus',
			'add_new_item'      => 'lisa uus valmidus',
			'new_item_name'     => 'uue valmiduse nimi',
			'menu_name'         => 'Valmidused'
		  );
		register_taxonomy('valmidus', array('terviserada'), array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'valmidus' ),
		));
	}
}

add_action('init',  function(){
	new EtrCustomPostTypes();
});