<?php

define(GOOGLE_MAPS_V3_API_KEY, 'AIzaSyAcocFru9YUHS17RjQv_KPnu7UGRPgwH6s');
define(IMAGEFOLDER, get_stylesheet_directory_uri() . '/images/');

/*===============================================
=            include necessary files            =
===============================================*/

include 'register_post_type.php';
include 'etr-metaboxes.php';

/*-----  End of include necessary files  ------*/
/**
*
* Lets register scripts
*
**/


wp_register_script( 'home-sponsors', get_stylesheet_directory_uri() . '/js/front-sponsors.js', array('jquery') );
wp_register_script( 'mainjs', get_stylesheet_directory_uri() . '/js/etr_responsive.js', array('jquery'));
wp_register_script( 'backstretch', get_stylesheet_directory_uri() . '/js/backstretch.js', array('jquery'));
wp_register_script( 'etr2', get_stylesheet_directory_uri() . '/js/etr2.js', array('jquery'), '1', true );
wp_register_script( 'wsdmap', get_stylesheet_directory_uri() . '/js/wsdmap.js', array('jquery'), '1', true );
wp_register_script( 'mapall', get_stylesheet_directory_uri() . '/js/map-all.js', array('jquery'), '1', true );
wp_register_script( 'googlemaps', 'http://maps.googleapis.com/maps/api/js?&key=' . GOOGLE_MAPS_V3_API_KEY . '&sensor=false&libraries=weather');

/**
*
* Lets Localize scripts
*
**/

$geopos    = 'etr_geopos';
$maakond   = 'etr_maakond';
$website   = 'etr_website';
$email     = 'etr_email';
$aadress   = 'etr_aadress';
$kontakt   = 'etr_kontakt';
$avatud    = 'etr_opentime';
$post_type = 'tervisekeskus';
$lon = 'etr_lon';
$lat = 'etr_lat';


$etr_all_data = $wpdb->get_results("SELECT
	p.id,
	p.post_title,
	p.post_content,
	pm1.meta_value AS $geopos,
	pm2.meta_value AS $maakond,
	pm3.meta_value AS $website,
	pm4.meta_value AS $email,
	pm5.meta_value AS $kontakt,
	pm6.meta_value AS $aadress,
	pm7.meta_value AS $avatud,
	pm8.meta_value AS $lon,
	pm9.meta_value AS $lat
	FROM $wpdb->posts p
	LEFT JOIN $wpdb->postmeta pm1 ON (p.id = pm1.post_id AND pm1.meta_key = '$geopos')
	LEFT JOIN $wpdb->postmeta pm2 ON (p.id = pm2.post_id AND pm2.meta_key = '$maakond')
	LEFT JOIN $wpdb->postmeta pm3 ON (p.id = pm3.post_id and pm3.meta_key = '$website')
	LEFT JOIN $wpdb->postmeta pm4 ON (p.id = pm4.post_id and pm4.meta_key = '$email')
	LEFT JOIN $wpdb->postmeta pm5 ON (p.id = pm5.post_id and pm5.meta_key = '$aadress')
	LEFT JOIN $wpdb->postmeta pm6 ON (p.id = pm6.post_id and pm6.meta_key = '$kontakt')
	LEFT JOIN $wpdb->postmeta pm7 ON (p.id = pm7.post_id and pm7.meta_key = '$avatud')
	LEFT JOIN $wpdb->postmeta pm8 ON (p.id = pm8.post_id and pm8.meta_key = '$lon')
	LEFT JOIN $wpdb->postmeta pm9 ON (p.id = pm9.post_id and pm9.meta_key = '$lat')
	WHERE p.post_type like '$post_type' and p.post_status like 'publish'

");

$etr_all = array();

$etr_posts_data = get_posts( array(
    'numberposts' =>    -1,
    'offset'      =>    0,
    'orderby'     =>    'post_date',
    'order'       =>    'DESC',
    'post_type'   =>    'tervisekeskus',
    'post_status' =>    'publish')
);

if( !empty($etr_posts_data) ){
    foreach($etr_posts_data as $place){
        $postmeta = get_post_meta($place->ID, $key = '', $single = false);
        $etr_all[$place->post_title]['name'] = $place->post_title;
        $etr_all[$place->post_title]['content'] = $place->post_content;
        $etr_all[$place->post_title]['lat'] = $postmeta[etr_lat][0];
        $etr_all[$place->post_title]['lon'] = $postmeta[etr_lon][0];
        $etr_all[$place->post_title]['county'] = $postmeta['etr_maakond'][0];
        $etr_all[$place->post_title]['ident'] = $postmeta['etr_ident'][0];
    }
}

$upload_arr = wp_upload_dir();
$js_data = array(
	'siteurl'  => get_option('siteurl'),
	'themeUri' => get_stylesheet_directory_uri(),
	'uploads'  => $upload_arr
	);

wp_localize_script('backstretch', 'wp', $js_data);
wp_localize_script('mainjs', 'wp', $js_data);
wp_localize_script('wsdmap', 'wp', $js_data);
wp_localize_script('etr2', 'etrAll', $etr_all);
wp_localize_script('etr2', 'etrAllPosts', $etr_posts_data);
wp_localize_script('mapall', 'etrPost', $etr_all);
wp_localize_script('mapall', 'wp', $js_data);

/**
*
* Lets add language text domain
*
**/

function language_setup() {
	load_child_theme_textdomain( 'terviserajad', get_stylesheet_directory_uri() . '/languages' );
}

add_action( 'after_setup_theme', 'language_setup' );



/**
*
* initialize thickbox
*
**/

// add_action('init', 'myplugin_thickbox');
// function myplugin_thickbox() {
// 	if (! is_admin()) {
// 		wp_enqueue_script('thickbox', null,  array('jquery'));
// 		wp_enqueue_style('thickbox.css', '/'.WPINC.'/js/thickbox/thickbox.css', null, '1.0');
// 	}
// }


/*=========================================
=            gmapEtr shortcode            =
=========================================*/

add_shortcode('gmap', function($atts){
	return '<div id="map_canvas_all"></div>';

});

/*-----  End of gmapEtr shortcode  ------*/


