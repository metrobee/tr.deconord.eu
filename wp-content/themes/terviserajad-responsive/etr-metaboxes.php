<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign
$prefix = 'etr_';

global $meta_boxes;

$meta_boxes = array();

// 1st meta box
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'aadressi_info',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => 'Aadressi info',

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'tervisekeskus' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// List of meta fields
	'fields' => array(

		// Postiaadress
		array(
			'name' => 'aadress',
			'id'   => "{$prefix}aadress",
			'type' => 'textarea',
			'clone' => false,
			'desc'	=> 'Sisestage Tervisepaiga Postiaadress',
			'cols' => '20',
			'rows' => '3',
		),

		// Maakond
		array(
			'name'     => 'Maakond',
			'id'       => "{$prefix}maakond",
			'type'     => 'text',
			'desc'		=> 'Määratle maakond'
		),
	)
);

// 2nd meta box
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'kontakt_info',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => 'Kontaktinfo',

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'tervisekeskus' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	'fields' => array(
		// Kontaktisik
		array(
			'name' => 'Asutus / Haldaja',
			'id'   => "{$prefix}kontakt",
			'type' => 'text',
			'desc'	=> 'Asutus/Haldusettevõte'
		),
		// Telefon
		array(
			'name' => 'Tel',
			'id'   => "{$prefix}tel",
			'type' => 'text',
			'clone' => true,
			'desc'	=> 'Sisesta üks või mitu telefoninumbrit. +märgile klikkides saad lisada uue numbri'
		),

		// E-post
		array(
			'name' => 'Email',
			'id'   => "{$prefix}email",
			'type' => 'text',
			'clone' => true,
			'desc'	=> 'Sisestage üks või mitu e-posti aadressi. +märgile klikkides saad lisada uue aadressi'
		),
		// website url
		array(
			'name' => 'Veebileht',
			'id'   => "{$prefix}website",
			'type' => 'text',
			'desc'	=> 'Sisestage veebilehe aadress'
		),

		// Lat
		array(
			'name' => 'Laiuskraad',
			'id'   => "{$prefix}lat",
			'type' => 'text',
			'desc'	=> 'Sisestage laiuskraad (6 kohta peale punkti on piisav)'
		),

		// Lon
		array(
			'name' => 'Pikkuskraad',
			'id'   => "{$prefix}lon",
			'type' => 'text',
			'desc'	=> 'Sisestage pikkuskraad (6 kohta peale punkti on piisav)'
		),
	)
);

// Sotsiaalmeedia
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'sotsiaalmeedia',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => 'Sotsiaalmeedia',

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'tervisekeskus' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'side',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'low',
		'fields' => array(

		// Twitter
		array(
			'name' => 'Twitter',
			'id'   => "{$prefix}twitter",
			'type' => 'text',
			'desc'	=> 'Sisesta olemasolu korral Twitteri konto nimi'
		),

		// Facebook
		array(
			'name' => 'Facebook',
			'id'   => "{$prefix}facebook",
			'type' => 'text',
			'desc'	=> 'Sisesta olemasolu korral Facebook konto nimi'
		),

		// Foursquare
		array(
			'name' => 'Foursquare',
			'id'   => "{$prefix}foursquare",
			'type' => 'text',
			'desc'	=> 'Sisesta olemasolu korral Foursquare konto'
		)
	)
);

// World Snow Day 2013
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'wsd2013',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => 'World Snow Day 2013',

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'tervisekeskus' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'low',
		'fields' => array(
		// wsd päeva pealkiri
		array(
			'name' => 'päeva pealkiri',
			'id'   => "{$prefix}wsdheading",
			'type' => 'text',
			'desc'	=> 'Sisesta lisa nimetus nt. Tartu Kaubamaja uisupäev'
		),
		// wsd text
		array(
			'name' => 'Ürituse info',
			'id'   => "{$prefix}wsdtext",
			'type' => 'wysiwyg',
			'std'  => 'Sisesta ürituse kohta info',

			// Editor settings, see wp_editor() function: look4wp.com/wp_editor
			'options' => array(
				'textarea_rows' => 4,
				'teeny'         => true,
				'media_buttons' => false,
			),
		),

		// wsd website
		array(
			'name' => 'veebileht',
			'id'   => "{$prefix}wsdurl",
			'type' => 'text',
			'desc'	=> 'Sisesta olemasolu link välislehele'
		),

		array(
			'name'             => 'Pilt/pildid',
			'id'               => "{$prefix}plupload",
			'type'             => 'plupload_image',
			'max_file_uploads' => 4,
		),
	)
);
// terviserajad meta box
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'infopanel',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => 'Geoinfo',

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'terviserada' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'side',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'default',
		'fields' => array(

		// geodata
		array(
			'name' => 'Geoinfo',
			'id'   => "{$prefix}geoarray",
			'type' => 'textarea',
			'desc'	=> 'Sisesta gpx failist geoandmed massiivina'
		),
	)
);

/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function YOUR_PREFIX_register_meta_boxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Meta_Box' ) )
		return;

	global $meta_boxes;
	foreach ( $meta_boxes as $meta_box )
	{
		new RW_Meta_Box( $meta_box );
	}
}
// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'YOUR_PREFIX_register_meta_boxes' );