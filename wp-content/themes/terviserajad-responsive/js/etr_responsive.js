(function($) {

// $() will work as an alias for jQuery() inside of this function
$('#footer').html('<div id="sponsors"><span>suurtoetajad: </span><a href="http://www.merko.ee" target="_blank"><div class="merko"></div></a><a href="http://www.energia.ee" target="_blank"><div class="energia"></div></a><a href="http://www.swedbank.ee" target="_blank"><div class="swedbank"></div></a></div>');
var countyList = $('div.counties-page'),
	map = $('#etrMap').addClass('etrmap-page');

countyList.appendTo(map);

var countyPlaces = {

	init: function(){
		var county = $('ul.etrCounty > li'),
			places = $('ul.etrPlace');
		places.hide();

		$(county).css('cursor','pointer').on('click' , function(){
			$(this).find(places)
			.clone()
			.prepend(  $(this).html() )
			.appendTo(map)
			.addClass('list-etr-places')
			.animate({'width':'toggle', 'height':'toggle'})
			.find('li').hover( function(){
				var className = 'a.' + $(this).attr('class');
				// console.log(className);
				$(className).children('img').attr('src', wp.themeUri +'/images/etr_c.png').animate({top:'2px'},'swing').addClass('etr-hover');
			} , function(){
				$('.etr-hover').attr('src', wp.themeUri + '/images/etr_a.png');
			}).end()
			.siblings('ul.etrPlace')
			.remove();
		});
	}
};

countyPlaces.init();

})(jQuery);