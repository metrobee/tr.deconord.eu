function initWsdMap(){

		mapOptions = {
		center: new google.maps.LatLng(58.55,24.8241709),
		zoom: 7,
		minZoom: 7,
		maxZoom: 9,
		mapTypeId: google.maps.MapTypeId.ROADMAP
};

var map = new google.maps.Map(document.getElementById("wsd-map"),
	mapOptions);

for(var x in wsdData){
	var lat = wsdData[x].lat,
		lon = wsdData[x].lon,
		location = new google.maps.LatLng(lat, lon),
		place = wsdData[x];
	addMarker(map, place, location );
}

function addMarker(map, place, location){
	var marker = new google.maps.Marker({
		position: location,
		map: map,
		title: place.name,
		icon: wp.themeUri + '/images/wsdflakeicondarkblue.png'
	});

	google.maps.event.addListener(marker, 'click', function() {
		if( typeof infowindow != 'undefined') infowindow.close();
        infowindow = new google.maps.InfoWindow({
			content: '<h4>' + place.wsdtitle + '</h4>'+
				'<div class="wsd-content"><a class="thickbox" href="' + place.wsdimgsrc[0]+ '" >' + place.wsdimg + '</a>'+
				place.wsdcontent + '<a href="' + wp.siteurl +'/tervisespot/?etrid='+ place.ident +'">Tervisekeskuse üldinfo</a></div>'
		});

        if( typeof(place.wsdtitle) != 'undefined' && place.wsdtitle !== null ){
        infowindow.open(map,marker);
        } else{infowindow.content = '<h5>Lumepäeva info varsti tulekul!</h5>...naudi seni head talveilma!';
        infowindow.open(map,marker);}
    });

	}
}

initWsdMap();