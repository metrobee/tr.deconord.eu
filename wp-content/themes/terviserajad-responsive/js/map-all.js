function initMapAll() {

	var mapOptions = {
		center: new google.maps.LatLng(58.55,24.8241709),
		zoom: 6,
		minZoom: 6,
		maxZoom:9,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById("map_canvas_all"),
		mapOptions);

	for(var x in etrPost){
		var lat = etrPost[x].lat,
			lon = etrPost[x].lon,
			location = new google.maps.LatLng(lat, lon),
			place = etrPost[x];
		addMarker(map, place, location );
	}

	function addMarker(map, place, location){
		var marker = new google.maps.Marker({
			position: location,
			map: map,
			title: place.name,
			icon: wp.themeUri + '/images/terviserajad-e.png'
		});

		google.maps.event.addListener(marker, 'click', function() {
			if( typeof infowindow != 'undefined') infowindow.close();
			infowindow = new google.maps.InfoWindow({
				content: '<h4>' + place.name + '</h4>'+
				'<div class="place-content" ><p>'+ place.content +
				'</p><a href="' + wp.siteurl +'/tervisespot/?etrid='+ place.ident +'">Tervisekeskuse üldinfo</a></div>'
			});

			infowindow.open(map,marker);
		});

	}

}

google.maps.event.addDomListener(window, 'load', initMapAll);