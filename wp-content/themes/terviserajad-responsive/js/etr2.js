(function($) {
    // $() will work as an alias for jQuery() inside of this function

    var lt = etrPostMeta.etr_lat,
		ln = etrPostMeta.etr_lon;

	function init_etr() {

			mapOptions = {
			center: new google.maps.LatLng(lt,ln),
			zoom: 8,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById("map_canvas"),
		mapOptions);

	var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lt,ln),
        map: map
        // title: postData.name
    });
    var contentString = 'hi. read more below <a href="http://teerada.dev/wp-content/uploads/2013/01/a3_plakat.jpg" class="thickbox">kliki</a>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    google.maps.event.addListener(marker, 'mouseover', function() {
           infowindow.open(map,marker);
        });
}

	function init_etr_all() {

			mapOptions = {
			center: new google.maps.LatLng(59.110966,24.8241709),
			zoom: 7,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map2 = new google.maps.Map(document.getElementById("map_canvas_all"),
		mapOptions);

	for(var x in etrAll){
		var lat = etrAll[x].etr_lat,
			lon = etrAll[x].etr_lon;
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat,lon),
			map: map2,
			title: etrAll[x].post_title
		});
	}
	var weatherLayer = new google.maps.weather.WeatherLayer({
		temperatureUnits: google.maps.weather.TemperatureUnit.CELCIUS
	});
	weatherLayer.setMap(map2);
	var cloudLayer = new google.maps.weather.CloudLayer();
	cloudLayer.setMap(map2);

	var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading">' + etrPostMeta.etr_maakond +'</h1>'+
            '<div id="bodyContent">'+
            '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
            'sandstone rock formation in the southern part of the '+
            'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
            'south west of the nearest large town, Alice Springs; 450&#160;km '+
            '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
            'features of the Uluru - Kata Tjuta National Park. Uluru is '+
            'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
            'Aboriginal people of the area. It has many springs, waterholes, '+
            'rock caves and ancient paintings. Uluru is listed as a World '+
            'Heritage Site.</p>'+
            '<p>Attribution: Uluru, <a href="http://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
            'http://en.wikipedia.org/w/index.php?title=Uluru</a> '+
            '(last visited June 22, 2009).</p>'+
            '</div>'+
            '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });


	}

	init_etr();

     $('#map_canvas').find('button').on('click', function(){
        console.log('kiisu');
    });


})(jQuery);


