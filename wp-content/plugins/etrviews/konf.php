<?php
session_start();
//* LEHEKÜLJE seaded, NB! muuta enne serverisse panekut
	$Db["table"]="etr_";				// *
	$Env["table"]="etr_";
	$Env["sessSecret"]="5649828363298898746";
// rakenduse seaded
	$lumeinfoUrl="http://arhiiv.sport.err.ee/lumeinfo.php?0&id=";
	$ilmeeIlmUrl="http://tervise:rajad@ilm.ee/~data/include/paring_xml.php3";
	$ilmeeClouds="http://www.ilm.ee/images/2008_2/weather/small/";
	$iconWidth=57;
	$iconTop=0;
	$BingMapKey="Akc3ga1mT4PNKTWoXk7HTYOb-FsJ5tNPhOTF2Y9iep_TLRz0lxcTk4XpS10GWlaz";
	$countyArray =array("Harjumaa"=>array("Harjumaa",59.3527, 24.8837, 9),"Hiiumaa"=>array("Hiiumaa",58.8941, 22.6521, 9),
	"Ida-Virumaa"=>array("Ida-Virumaa",59.2540, 27.3940, 9),"Jõgevamaa"=>array("Jõgevamaa",58.750,26.520, 9),
	"Järvamaa"=>array("Järvamaa",58.9070, 25.6348, 9),"Lääne-Virumaa"=>array("Lääne-Virumaa",59.2607, 26.3310, 9),
	"Läänemaa"=>array("Läänemaa",58.9263, 23.5997, 9),"Põlvamaa"=>array("Põlvamaa",58.0640, 27.1427, 9),
	"Pärnumaa"=>array("Pärnumaa",58.3940, 24.5219, 9),"Raplamaa"=>array("Raplamaa",58.9697, 24.7036, 9),
	"Saaremaa"=>array("Saaremaa",58.3913, 22.4060, 9),"Tartumaa"=>array("Tartumaa",58.3748, 26.7279, 9),
	"Valgamaa"=>array("Valgamaa",57.9251, 26.1584, 9),"Viljandimaa"=>array("Viljandimaa",58.3765, 25.6181, 9),
	"Võrumaa"=>array("Võrumaa",57.7677, 26.9339, 9));
				// "tunnus" => (ikooni jrk, vaikimi tekst), 
	$iconArray =array(
	"toitlustus"=>array(0,"Toitlustus",0),
	"majutus"=>array(1,"Majutus",58),
	"parkimine"=>array(2,"Parkimine",114),
	"pesemine"=>array(3,"Pesemine/saun",171),	
	"lumi"=>array(4,"Kunstlumi",228),
	"laenutus"=>array(5,"Laenutus",284),
	"valgustus"=>array(6,"Valgustatud rada",342),
	"suusk"=>array(7,"Suusa rajad",399),	
	"rull"=>array(8,"Rulli rajad",456),
	"matk"=>array(9,"Matka rajad",513),
	"ratas"=>array(10,"Ratta rajad",570),
	"mobo"=>array(11,"(MOBO)orienteerumine",624),
	"voimlemine"=>array(12,"Võimlemine",681),
	"viidad"=>array(13,"Viidad/tähistus",738),	
	"joud"=>array(14,"Jõulinnak",795),	
	"discgolf"=>array(15,"DiscGolf",852),	
	"jooks"=>array(16,"Jooksu rajad",912)
	);
	$badChars= array("\r","\n","\r\n");
	$trackColors = array(
	"#FF0000"=>"Punane",
	"#000000"=>"Must",
	"#0026FF"=>"Sinine",
	"#007F46"=>"Roheline",
	"#7F3300"=>"Pruun",
	"#FFD800"=>"Kollane",
	"#B200FF"=>"Valge"
	);

function html_icon($jrk,$name,$url,$tekst,$baseURL){
	$ikoon=$GLOBALS['iconArray'][$name];
	if ($tekst==""){
		$tekst=$ikoon[1];
	}
	//$Icpos=$ikoon[0]*$GLOBALS['iconWidth'];
	$Icpos=$ikoon[2];
	if ($url==""){
		$iconImg= "<li style=\"left:".$jrk*$GLOBALS['iconWidth']."px; width:57px; background-position-y: 0px; background-position-x: -{$Icpos}px;\"><a title=\"{$tekst}\"></a>";
		} else {
		$iconImg= "<li style=\"left:".$jrk*$GLOBALS['iconWidth']."px; width:57px; background-position-y: 0px; background-position-x: -{$Icpos}px;\"><a href=\"{$url}\" targe=\"outUrl\" title=\"{$tekst}\"></a>";
		}
	if ($baseURL!="NOLI") { $iconImg.="<li>";}
	return $iconImg;
}
?>