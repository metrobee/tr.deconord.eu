<?php
include_once('konf.php');
class ETRForWordpressContent
{
	/*
	 * Keskuse DetailView aadress
	 * @var string
	 * @access public
	 */
	var $pageUrl;

	/**
	 * Vaate parameetrid
	 * @var array
	 * @access public
	 */
	var $atts = array();

	/**
	 * Static map types
	 * @var array
	 * @access public
	 */
	var $pagetypes = array(
		'map' => 'Kaart',
		'list' => 'Nimekiri',
		'show' => 'Keskuse info'
	);

	var $debug = false;
//	var $cDB;
	var $baseurl;
	/**
	 * PHP5 constructor
	 * @access public
	 * @return void
	 */
	function __construct(){ $this->ETRForWordpressContent();}

	/**
	 * PHP4 constructor
	 * @access public
	 * @return void
	 */
	function ETRForWordpressContent()
	{
		// Get options
		$options = get_option('etr_for_wordpress');
		$this->baseurl = WP_PLUGIN_URL.'/'.basename(dirname(__FILE__)).'/';
		// Only run if we have an API key
		if(isset($options['pageUrl']) AND $options['pageUrl'] != '')
		{
			// Header action - to load JS scriptid
			add_action('wp_head', array($this, '__header'));
			// Set the Url
			$this->pageUrl = $options['pageUrl'];
			// Add shortcode handler
			add_shortcode('etrView', array($this, 'shortcode'));
		}
	}

	/**
	 * Handle [etr] shortcode - make sure settings are all correct
	 * @param array $atts
	 * @access public
	 * @return string
	 */
	function shortcode($atts)
	{
		// Encode view tyoe
		if(isset($atts['view'])) $atts['view'] = rawurlencode($atts['view']);
		//if(isset($atts['id'])) $atts['id'] = rawurlencode($atts['id']);
		extract(shortcode_atts(array('id'=>'1'),$atts));
		$this->atts = $atts;

		$method = '__displayETRMap';
		switch ($atts['view']){
			case "map":
				$method = '__displayETRMap';
				break;
			case "list":
				$method = '__displayETRList';
				break;
			case "show":
				$method = '__displayETRShowDetails';
				if (!isset($this->atts['id']) && !isset($_GET['etrid'])) {
					$method = '__displayETRList';
				}
				break;
		}
		return $this->{$method}();
	}

	/**
	 * Display a  map with locations
	 * @access private
	 * @return string
	 */
	function __displayETRList()
	{
		$string="";
		global $Db;
		global $wpdb;
		$dbList= $wpdb->get_results("SELECT id, name, county FROM {$Db['table']}maininfo ORDER BY county, name", OBJECT);
		$string.="<ul class=\"etrCounty\">";
		foreach ($GLOBALS["countyArray"] as $key => $value){
			$string.= "<li><div class=\"lListClick\" onclick=\"countyCenter({$value[1]},{$value[2]},{$value[3]});\">{$value[0]}</div>";
			$string.= "<div id=\"etr_".$key."\" class=\"lList\"><ul class=\"etrPlace\">";
			foreach($dbList as $data){
 				if ($data->county==$key){
					$string.= "<li class=\"etr-$data->id\"><a href=\"{$this->pageUrl}?etrid={$data->id}\">{$data->name}</a></li>";
				}
			}
			$string.= "</ul></div>";
		}
		$string.="</ul>";
		return $string;
	}

	function __displayETRShowDetails()
	{
		$string="";
		$etr_id="";
		if (isset($this->atts['id'])){
			$etr_id=$this->atts['id'];
		}

		if (isset($_GET['etrid'])) {
			$etr_id=rawurlencode($_GET['etrid']);
		}
		$etr_id=(int)$etr_id;
		if ($etr_id!=0) {
		global $Db;
		global $wpdb;
		$sql="SELECT * FROM {$Db['table']}maininfo WHERE id={$etr_id}";
		$trackData=$wpdb->get_row($sql, ARRAY_A);

		if ($trackData != null) {
		$string="<script type=\"text/javascript\">\n".
			"var pinLoc=new Microsoft.Maps.Location(".$trackData['geo_pos'].");\n".
			"var gfx='".$this->baseurl."';".
			"var pushpinCenter = {icon: '".$this->baseurl."etr_c.png', width:15, height:16};\n".
			"var pushpinStart = {icon: '".$this->baseurl."etr_s.png', width:16, height:16};\n".
			"var pushpinKMstyle = {icon: '".$this->baseurl."etr_km.png', width:15, height:16, typeName: 'pinKMpost'};\n".
			"function GetETRMap(){ \n".
			"Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', { callback: function () {".
			"map = new Microsoft.Maps.Map(document.getElementById(\"etrMap\"),".
			 "{credentials:'".$GLOBALS["BingMapKey"]."', ".
			 "showScalebar: false, ".
			 "showMapTypeSelector: false, ".
			 "enableSearchLogo: false,".
			 "theme: new Microsoft.Maps.Themes.BingTheme()}); \n".
			"map.setView({ zoom: 11, ".
			"center: pinLoc,".
			"mapTypeId: Microsoft.Maps.MapTypeId.automatic });\n".
			"map.entities.clear(); \n".
			"var pushpin = new Microsoft.Maps.Pushpin(pinLoc, {icon: '".$this->baseurl."etrlipp.png', width: 110, height: 67, typeName: 'pinSLipp', text : '', visible: true});\n".
			"map.entities.push(pushpin); }});}\n".
			"$(document).ready(function() {GetETRMap();});";

			$string.="</script><h1>".$trackData['name']."</h1><table>";
			if ($trackData['description']!=""){
				$string.="<tr><td colspan=\"2\">".$trackData['description']."</td></tr>";
			}
			$string.="<tr><td class=\"tdTitle\">Maakond:</td><td>".$trackData['county']." (".$trackData['geo_pos'].")</td></tr>";
			if ($trackData['address']!=""){
				$string.="<tr><td class=\"tdTitle\">Aadress:</td><td>".str_replace($GLOBALS["badChars"],"<br>",$trackData['address'])."</td></tr>";
			}
			if ($trackData['opentime']!=""){
				$string.="<tr><td class=\"tdTitle\">Avatud:</td><td>".$trackData['opentime']."</td></tr>";
			}
			if ($trackData['contact']!=""){
				$string.="<tr><td class=\"tdTitle\">Kontakt:</td><td>".str_replace(array("\r\n"),"<br>",$trackData['contact'])."</td></tr>";
			}
			if ($trackData['link_email']!=""){
				$string.="<tr><td class=\"tdTitle\">E-post:</td><td>".str_replace("@","((a))",$trackData['link_email'])."</td></tr>";
			}
			if ($trackData['link_web']!=""){
				$kodukas=$trackData['link_web'];
			if (substr($kodukas,4)!="http") { $kodukas="http://".$kodukas;}
				$string.="<tr><td class=\"tdTitle\">Kodulehekülg:</td><td><a href=\"".$kodukas."\" target=\"outURL\">".$trackData['link_web']."</td></tr>";
			}
			$string.="<tr><td class=\"tdTitle\">Kohalik ilm:</td><td>";
// Ilm ee  temperatuur ~2+3h pärast.
			libxml_use_internal_errors(true);
			if($trackData['geo_pos']!=""){
			list($ilmY,$ilmX)=explode(",",$trackData['geo_pos'],2);
			$ilmaLink=$GLOBALS["ilmeeIlmUrl"]."?X={$ilmX}&Y={$ilmY}";
			$xml = simplexml_load_file($ilmaLink);
			$unixTime=time()+(5*3600);
			$stringIlm="";
			foreach($xml->{'blokk'} as $xmlIlm)
			{
//				print_r($xmlIlm);
				if ($unixTime>=$xmlIlm->{'aeg'}){
					$stringIlm="<img src=\"".$GLOBALS["ilmeeClouds"].$xmlIlm->{'pilvefail'}."\" alt=\"\" />";
					$stringIlm.=$xmlIlm->{'temp'}."&#8451;, tuul:".$xmlIlm->{'kiirus'}."m/s, niiskus:".$xmlIlm->{'niiskus'}."%&nbsp;";
				}
			}
			$string.=$stringIlm;
			if ($trackData['link_ilmee']!=""){
				$string.="<a href=\"".$trackData['link_ilmee']."\" target=\"outURLIlm\">Ilm.ee ilmateade</a>";
			}
			}
			$string.="</td></tr>";
			if ($trackData['link_lumeinfo']!=""){
				$string.="<tr><td class=\"tdTitle\">Lumeinfo:</td><td><a href=\"".$GLOBALS["lumeinfoUrl"].$trackData['link_lumeinfo']."\" target=\"outURLLumi\">Lumeinfo ERRi portaalis</td></tr>";
			}
			$string.="<tr></table>";
// Lumeinfo tekstina (XML)	TODO ajaxiga laadida, ainult siis kui info on pädev	(uuem kui 14p)
			if ($trackData['link_lumeinfo']!="") {
				$lumeInfo="http://arhiiv.sport.err.ee/ext/trackinfo.php?id=".$trackData['link_lumeinfo'];
				$xml = simplexml_load_file($lumeInfo);
				// updated:2012 12 13 23 11 34
				if (isset($xml->{'updated'}) && $xml->{'updated'}>date('Ymd',time()-(14*24*60*60))."235959"){
					$string.="<h3>Rajainfo ERR lumeinfo portaalist:</h3><div id=\"lumeinfo\">";
					$string.=str_replace("\n","<br>",$xml->{'info'});
					$string.="</div>";
				}
		}

// Võimaluste nimekiri ikoonidena
			$sql=$wpdb->prepare("SELECT * FROM {$Db['table']}mainicons WHERE main_id=%d ORDER BY groupline",$etr_id);
			$dbIcons= $wpdb->get_results($sql, ARRAY_A);
			if (count($dbIcons)>0) {
				$string.="<h3>Võimalused</h3><ul class=\"iconnav\">";
				$i=0;
				foreach($dbIcons as $rIcon){
					if ($i % 10==9) {
						$string.="</ul><ul class=\"iconnav\">";
						$i=0;
					}
					$string.=html_icon($i++,$rIcon['typename'],$rIcon['url'],$rIcon['descript'],$this->baseurl);
				}
				$string.="</ul>";
			}
// Lumekihi paksus ilm.ee'st (novembrist, aprillini) nb! ilm.ee's peab registreerima domeeni?
			$currentMonth=date("n");
			if (($currentMonth>10 || $currentMonth<5) && true) {
				$string.="<h3>Lumekatte paksus</h3>";
				$string.= "<img src=\"http://www.ilm.ee/~data/include/meteogramm_suusad.php3?koht=(".str_replace(",",",%20",$trackData['geo_pos']).")\">";
			}
// Kaart radadega
			$sql=$wpdb->prepare("SELECT id, track_name, colour, track_data FROM {$Db['table']}track WHERE main_id=%d ORDER BY track_name",$etr_id);
			$dbTracks= $wpdb->get_results($sql, ARRAY_A);

			$string.="<h3>Kaart ja rajad</h3>";
			if (count($dbTracks)>0) {
				$string.="<div><ul class=\"trackList\">";
				foreach($dbTracks as $track){
					$string.="<li><div onClick=\"ShowTrack(".$track['id'].")\" class=\"trckname\" style=\"border-bottom-color: ".$track['colour']."\">".$track['track_name']."</div>";
					$sql="SELECT typename FROM {$Db['table']}trackicons WHERE track_id={$track['id']} ORDER BY typename";
					$dbTracksIcons= $wpdb->get_results($sql, ARRAY_A);
					if (count($dbTracksIcons)) {
						$string.="<ul class=\"iconnav\">";	$i=0;
						foreach($dbTracksIcons as $tIcon){
							$string.=html_icon($i++,$tIcon['typename'],"","",$this->baseurl);
						}
						$string.="</ul>";
					}
					$string.="</li><script type=\"text/javascript\">trck[".$track['id']."]=new Array(".$track['track_data']."); color[".$track['id']."]='".$track['colour']."';</script>";
				}
				$string.="</ul><div>";
			}
			$string.="<div id=\"etrMap\" style=\"position: relative; width: 100%; height:350px\"></div><br>";
			$string.="<div id=\"jqxProfiil\" style=\"width:100%; height:150px; border-style:none;\"></div><br>";
		} }
		return $string;
	}

	function __displayETRMap()
	{
		global $Db;
		global $wpdb;
		$defZoom=6;
		if (isset($this->atts['zoom'])){
			$defZoom=$this->atts['zoom'];
		}
		$defStyle="position: relative;";
		if (isset($this->atts['istyle'])){
			$defStyle=$this->atts['istyle'];
		}

		$string="<script type=\"text/javascript\">function ShowInfo(id)\n".
		"{	window.location ='".$this->pageUrl."?etrid='+id;}\nfunction GetETRMap()\n{".
		"Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', { callback: function () {".
		"map = new Microsoft.Maps.Map(document.getElementById(\"etrMap\"),".
		"{credentials:'".$GLOBALS["BingMapKey"]."', ".
		"showScalebar: false, ".
		"showMapTypeSelector: false, ".
		"enableSearchLogo: false,".
		"theme: new Microsoft.Maps.Themes.BingTheme()}); ".
		"map.setView({ zoom: ".$defZoom.", ".
		"center: new Microsoft.Maps.Location(58.5970, 24.9280),".
		"mapTypeId: Microsoft.Maps.MapTypeId.automatic });".
		"map.entities.clear(); ".
		"var pushpinOptions = { icon: '".$this->baseurl."etr_c.png', width:15, height:16 };";
		$sql="SELECT id, name, county, geo_pos, description FROM {$Db['table']}maininfo WHERE geo_pos<>'' and geo_pos<>'ei tea' ORDER BY name";
		$dbList= $wpdb->get_results($sql,ARRAY_A);
		foreach($dbList as $row){
			$string.= "var pinLoc".$row['id']."=new Microsoft.Maps.Location(".$row['geo_pos'].");";
			$string.= "var pushpin".$row['id']." = new Microsoft.Maps.Pushpin(pinLoc".$row['id'].", {icon: '".$this->baseurl."etr_a.png', width: 15, height: 15, typeName: 'etr-".$row['id']."',text: '', visible: true}); ";
			$string.= "map.entities.push(pushpin".$row['id']."); ";
			$string.= "var rada".$row['id']."= { title: '".$row['name']."',description: '".str_replace($GLOBALS["badChars"],"<br>",$row['description'])."<br><div onClick=\"ShowInfo(".$row['id'].")\">Info</div>',pushpin: pushpin".$row['id']."};";
			$string.= "map.entities.push(new Microsoft.Maps.Infobox(pinLoc".$row['id'].", rada".$row['id']."));\r\n";
		}
		$string.="}});}\n $(document).ready(function() {GetETRMap();});</script>";
		$string.="<div id=\"etrMap\" style=\"".$defStyle."\"></div>";
		return $string;
	}

	/**
	 * JS controls
	 * @access private
	 * @return void
	 */
	function __header()
	{
		// ETRi stiilid
		$urlc = $this->baseurl.'etrstyles.css';
		echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$urlc."\" />\n";
		// JS
		$urlj = $this->baseurl.'etrviews.js';
		echo '<script type="text/javascript" src="'.$urlj.'"></script>';
		echo '<script type="text/javascript" src="'.$this->baseurl.'jquery-1.8.1.min.js"></script>';
		//echo '<script type="text/javascript" src="'.$this->baseurl.'jqxcore.js"></script>';
		//echo '<script type="text/javascript" src="'.$this->baseurl.'jqxdata.js"></script>';
		//echo '<script type="text/javascript" src="'.$this->baseurl.'jqxchart.js"></script>';
		echo '<script src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0" type="text/javascript"></script>';
	}
}