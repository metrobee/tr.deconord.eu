<?php
$pageError="";
include_once("konf.php");
include_once("mycdb.php");
include_once("funks.php");
include_once("ui_html.php");
$pageName="seaded";
?>
<!DOCTYPE html>
<html>
<head>
<title>ETR</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<!-- <link href="etradmin.css" media="all" rel="stylesheet" type="text/css" /> -->
<link rel="stylesheet" href="kuulind.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
<script charset="UTF-8" type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
<script type="text/javascript" src="etr.js"></script>
</head>

<body id="main_body">
	<header>
		<a href='seaded.php'><img src="etr-logo.jpg" alt="etr-logo"></a>
		<span class="page-info">ETR rajad | administraator</span>
	</header>

	<div id="form_container">
		<?php
		if ($pageError!=""){
			echo "<div id='error_message' class='pageError'>".$pageError."</div>";
		}
		$pContent= new cPage("PageView");
		$pContent->env=$Env;
		$pContent->showContent($cDB,$pageName);
		?>

	</div>
	<div id="footer"> Eesti Terviserajad 2013</div>
	<script src="kuulind.js"></script>
</body>
</html>
