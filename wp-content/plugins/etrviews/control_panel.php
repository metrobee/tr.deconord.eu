<?php
include_once('konf.php');
class ETRForWordpressControlPanel
{
	/**
	 * PHP5 constructor - links to old style PHP4 constructor
	 * @param string $file
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	function __construct($file)
	{
		$this->ETRForWordpressControlPanel($file);
	}
	
	/**
	 * Old style PHP4 constructor
	 * @param string $file
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	function ETRForWordpressControlPanel($file)
	{
		// Add Settings link to plugin page
		add_filter("plugin_action_links_".$file, array($this, 'actlinks'));
		// Any settings to initialize
		add_action('admin_init', array($this, 'adminInit'));
		// Load menu page
		add_action('admin_menu', array($this, 'addAdminPage'));
		// Load admin CSS style sheet
		add_action('admin_head', array($this, 'registerHead'));
	}
	
	/**
	 * Add a setting link to the plugin settings page
	 * @param array $links
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	function actlinks($links)
	{
		// Add a link to this plugins settings page
		$settings_link = '<a href="options-general.php?page=etrviews.php">Settings</a>'; 
		array_unshift($links, $settings_link); 
		return $links; 
	}
	
	/**
	 * Initialize admin
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	function adminInit()
	{
		register_setting('etrForWordpressOptions', 'etr_for_wordpress');
		
		// Check if we have a setting
		$options = get_option('etr_for_wordpress');
		
		if(!isset($options['pageUrl']) OR $options['pageUrl'] == '')
		{
			add_action('admin_head', array($this, 'initError'));
		}
	}
	
	/**
	 * Plugin initialization error - missing pageUrl key most likely
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	function initError()
	{																							   
		echo '<div class="updated"><p>ETRi plugi seadistamata! Seadista ETR for WordPress <a href="options-general.php?page=etr-for-wordpress">Settings</a> lehelt</p></div>';
	}
	
	/**
	 * Add an admin page to the general settings panel
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	function addAdminPage()
	{
		add_options_page('ETR for WordPress Options', 'ETR for Wordpress', 'administrator', 'etr_for_wordpress', array($this, 'admin'));
	}
	
	/**
	 * Admin page
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	function admin()
	{
		
		echo '<div class="wrap">';
		echo '<div class="half1">';
		echo '<form method="post" action="options.php">';
		
		echo '<h2>Eesti Terviseradade kaart for WordPress Settings</h2>';
		echo '<p><small>By: Tarmo Klaar</small></p>';
		echo '<table class="form-table" cellspacing="2" cellpadding="5">';
		
		settings_fields('etrForWordpressOptions');
		$options = get_option('etr_for_wordpress');
		
		echo '<tr>';
		
		echo '<th scope="row"><label>Sisesta radade kuvamise lehe aadress</label></th>';
		echo '<td><input type="text" class="regular-text" name="etr_for_wordpress[pageUrl]" value="'.$options['pageUrl'].'" />';
		echo '<br />Sellele aadressile suunatakse kui kaardilt või nimekirjast valitakse rada!';
		echo '</td>';
		echo '</tr>';
		
		echo '<tr><td colspan="2">  </td></tr>';
		
		echo '</table><br /><p class="submit"><input class="button-primary" type="submit" value="'.__('Save Changes').'" /></p>';
		echo '</form>			<h3>Kasutamine</h3>
					Siin väike juhend:<br>
					Kaardi kuvamiseks lehel, sisesta <br>[etrView view="map"]<br>
					Listi kuvamiseks lehel, sisesta<br> [etrView view="list"]<br><br>
					Raja info kuvamine, sisesta [etrView view="show" id="2"], kus id on raja number, ilma id väärtuseta võetakse id pängu urlist etrpageid=1<br>
				</div>
		
		<div class="half2">
		</div>
		</div>';
// 	*******************************************************************	
// ETR baasi administreerimine
		global $Env;
		global $cDB;
		$_SESSION['authPerson']='jaak';
		$_SESSION['authType']=$Env["sessSecret"];  	
		echo "<a href='".WP_PLUGIN_URL.'/'.basename(dirname(__FILE__))."/seaded.php'>Sisesta andmed ETRi baasi</a>";

// 	*******************************************************************
	}
	
	/**
	 * Add styles to admin header
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	function registerHead()
	{

	}
}