<?php 
$pageError="";
include_once("konf.php");
include_once("mycdb.php");
$cDB = new cDatabase($Db["db_host"], $Db["db_user"], $Db["db_password"], $Db["db_name"] , false);
$pageName="kaart";
?>
<!DOCTYPE html>
<html>
<head>
<title>ETR</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="Description" content="Eesti Terviserajad">
<meta name="Keywords" content="Terviserajad, jne">
<link href="gfx/styles.css" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/etr.js"></script>	
<script type="text/javascript" src="js/tinybox.js"></script>	
<script src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0" type="text/javascript"></script>
<script type="text/javascript">
function GetETRMap()
{
	// Initialize the map
	 Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', { callback: function () {
	map = new Microsoft.Maps.Map(document.getElementById("etrMap"),
	 {credentials:'<?php echo $BingMapKey;?>', 
	 showScalebar: false, 
	 showMapTypeSelector: false, 
	 enableSearchLogo: false,
	 theme: new Microsoft.Maps.Themes.BingTheme()}); 
	map.setView({ zoom: 7, 
	center: new Microsoft.Maps.Location(58.5970, 24.9280),
	mapTypeId: Microsoft.Maps.MapTypeId.automatic });
	map.entities.clear(); 
	var pushpinOptions = { icon: 'gfx/etrflag.png', width:46, height:15 };
<?php
	$sql="SELECT id, name, county, geo_pos, description FROM {$Env['table']}maininfo ORDER BY name";
	$rs=$cDB->ExecuteReader($sql);
	while ($row = @mysql_fetch_array($rs, MYSQL_ASSOC)) {
		echo "var pinLoc".$row['id']."=new Microsoft.Maps.Location(".$row['geo_pos'].");";
		echo "var pushpin".$row['id']." = new Microsoft.Maps.Pushpin(pinLoc".$row['id'].", {icon: 'gfx/etrflag.png', width: 46, height: 15, typeName: 'pinstyle', text : '', visible: true}); ";
		echo "map.entities.push(pushpin".$row['id']."); ";
        echo "var rada".$row['id']."= { title: '".$row['name']."',description: '".$row['description']."<div onClick=\"ShowInfo(".$row['id'].")\">--Info--</div>',pushpin: pushpin".$row['id']."};";
		echo "map.entities.push(new Microsoft.Maps.Infobox(pinLoc".$row['id'].", rada".$row['id']."));\r\n";
	}	
?>
		}
	});
}
</script>
</head>
<body id="main_body" onLoad="GetETRMap()">
<div id="form_container">
<h1>ETR rajad</h1>
<?php if ($pageError!=""){echo "<div id=\"error_message\" class=\"pageError\">".$pageError."</div>";} ?>	
<table>
	<tr>
	<td style="width: 550px; valign:top;">
	<div id="etrMap" style="position: relative; width: 545px; height: 400px;">
	</div>
	</td><td style="valign:top;">
	<ul>
<?php					
	$sql="SELECT id, name, county, geo_pos FROM {$Env['table']}maininfo ORDER BY county, name";
	$rs=$cDB->ExecuteReader($sql);
	while ($row[] = @mysql_fetch_array($rs, MYSQL_ASSOC));
	// print_r($row);
	foreach ($countyArray as $key => $value){
		echo "<li><div onclick=\"changeVisible('etr_".$key."');\" class=\"lListClick\">".$value[0]."</div>";
		echo "<div id=\"etr_".$key."\" class=\"lList\" style=\"display: none;\" >";
		foreach($row as $data){
			if ($data['county']==$key){
				echo "<div onClick=\"ShowLocation('{$data['geo_pos']}')\">{$data['name']}</div>";
				echo "<div class=\"\" onClick=\"ShowInfo('{$data['id']}')\">INFO</div>";
			}
		}
		echo "</div>";
	}
?></ul></td>
	</tr>
</table>	
	
<?php settype($rs, "null"); settype($cDB, "null");?>
<div id="footer"> Eesti Terviserajad 2013</div>
</div>
</body>

</html>