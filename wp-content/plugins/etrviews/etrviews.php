<?php
/*
Plugin Name: Eesti Terviseradade Radade info
Plugin URI: http://www.terviserajad.ee
Description: Eesti Terviseradade radade ja keskuste info kuvamine WordPressis
Version: 1.0
Author: Tarmo Klaar
Author URI: http://www.tak-soft.com
License: Private
*/

if(!is_admin())
{
	// Activate the plugin
	require_once('etrmain.php');
	new ETRForWordpressContent();
} else
{
	require_once('control_panel.php');
	new ETRForWordpressControlPanel(plugin_basename(__FILE__));
}
?>
