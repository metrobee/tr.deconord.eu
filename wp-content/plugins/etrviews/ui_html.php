<?php
class cPage {
	var $env;
	var $instanceName;
	var $pid;
	var $login;
	var $dbds;

	function __construct( $instanceName )
 	{
		$this->instanceName = $instanceName;
		$this->env			= array();
	 	$this->pid			= "";
	 	$this->login		= 0; // 0 - not loged in,  5xxxxx-admin
	 	$this->dbds			= null;
	}

//*****************************************
// *** COMMON HTML create functions
//*****************************************
function dateDiff($start, $end) {
	$start_ts = strtotime($start);
	$end_ts = strtotime($end);
	$diff = $end_ts - $start_ts;
	return round($diff / 86400);
}

function html_submitButton($val){
if ($val=="") {
$val="Salvesta";
}
?>	<li class="buttons">
	<input id="saveForm" class="button_text btn btn-primary" type="submit" name="submit" value="<?php echo $val; ?>" />
	</li>
<?php
}
function html_InputText($count,$label,$name,$value,$hint,$size){
	if ($size>20) { $s="large";} else if ($size>10) {$s="medium";} else {$s="small";}
?>
	<li id="li_<?php echo $count;?>" >
	<label class="description" for="element_<?php echo $count;?>"><?php echo $label;?> </label>
	<div>
		<input id="element_<?php echo $count;?>" name="<?php echo $name;?>" class="input-xxlarge element text <?php echo $s;?>" type="text" maxlength="<?php echo $size;?>" value="<?php echo $value;?>"/>
	</div><p class="guidelines" id="guide_<?php echo $count;?>"><small><?php echo $hint;?></small></p>
	</li>
<?php
}
function html_InputTextArea($count,$label,$name,$value,$hint,$rows){
?>
	<li id="li_<?php echo $count;?>" >
	<label class="description" for="element_<?php echo $count;?>"><?php echo $label;?> </label>
	<div>
		<textarea id="element_<?php echo $count;?>" name="<?php echo $name;?>" class="input-xxlarge element text small" cols="35" rows="<?php echo $rows;?>"><?php echo $value;?></textarea>
	</div><p class="guidelines" id="guide_<?php echo $count;?>"><small><?php echo $hint;?></small></p>
	</li>
<?php
}
function html_InputGeo($count,$label,$name,$value,$hint){
?>
	<li id="li_<?php echo $count;?>" >
	<label class="description" for="element_<?php echo $count;?>"><?php echo $label;?> </label>
	<div>
		<input id="element_<?php echo $count;?>" name="<?php echo $name;?>" class=" input-large element text medium" type="text" maxlength="21" value="<?php echo $value;?>"/>
	<input name="element_map_<?php echo $count;?>" id="imgBtnSelectMap" onclick="GetBingMap('pnlMap','element_<?php echo $count;?>');return false;" type="image" src="bing_btn.png" />
	<p class="guidelines" id="guide_<?php echo $count;?>"><small><?php echo $hint;?></small></p>
	<div id="pnlMap" style="width: 420px; height: 300px; left: 400px;  position: absolute;" />
	</li>
<?php
}
function html_Hidden($hiddenArr){
	foreach($hiddenArr as $key=>$value)
	{
	echo "<input type=\"hidden\" name=\"{$key}\" value=\"{$value}\" />";
	}
}
function html_DateTime($count,$label,$name,$value,$hint){
?>
<li id="li_<?php echo $count; ?>" >
	<label class="description" for="element_<?php echo $count; ?>"><?php echo $label;?></label>
	<span>
		<input id="element_<?php echo $count; ?>_1" name="<?php echo $name; ?>_dd" class="element text" size="2" maxlength="2" value="<?php echo substr($value,8,2); ?>" type="text"/> /
		<label for="element_<?php echo $count; ?>_1">DD</label>
	</span>
	<span>
		<input id="element_<?php echo $count; ?>_2" name="<?php echo $name; ?>_mm" class="element text" size="2" maxlength="2" value="<?php echo substr($value,5,2); ?>" type="text"/> /
		<label for="element_<?php echo $count; ?>_2">MM</label>
	</span>
	<span>
		<input id="element_<?php echo $count; ?>_3" name="<?php echo $name; ?>_yyyy" class="element text" size="4" maxlength="4" value="<?php echo substr($value,0,4); ?>" type="text"/>
		<label for="element_<?php echo $count; ?>_3">YYYY</label>
	</span>

	<span id="calendar_<?php echo $count; ?>">
		<img id="cal_img_<?php echo $count; ?>" class="datepicker" src="calendar.gif" alt="Vali kuupäev."/>
	</span>
	<span>
		<input id="element_<?php echo $count; ?>_4" name="<?php echo $name; ?>_hh" class="element text " size="2" type="text" maxlength="2" value="<?php echo substr($value,11,2); ?>"/> :
		<label>HH</label>
	</span>
	<span>
		<input id="element_<?php echo $count; ?>_5" name="<?php echo $name; ?>_hm" class="element text " size="2" type="text" maxlength="2" value="<?php echo substr($value,14,2); ?>"/>
		<label>MM</label>
	</span>
	<script type="text/javascript">
		Calendar.setup({
		inputField	 : "element_<?php echo $count; ?>_3",
		baseField    : "element_<?php echo $count; ?>",
		displayArea  : "calendar_<?php echo $count; ?>",
		button		 : "cal_img_<?php echo $count; ?>",
		ifFormat	 : "%B %e, %Y",
		onSelect	 : selectEuropeDate
		});
	</script>
	<p class="guidelines" id="guide_<?php echo $count; ?>"><small><?php echo $hint; ?></small></p>
</li>
<?php
}
function html_CheckBox($count,$label,$name,$value,$hint){
?>
	<input id="element_<?php echo $count;?>" name="<?php echo $name;?>" class="element checkbox" type="checkbox" value="1" <?php echo $value=="1"?"checked":"";  ?> >
	<label class="choice" for="element_<?php echo $count;?>"><?php echo $label;?></label>
<?php
}
function html_Choice($count,$label,$name,$value,$hint,$choicelist,$ax=1){
?>
	<li id="li_<?php echo $count; ?>" >
	<label class="description" for="element_<?php echo $count; ?>"><?php echo $label; ?></label>
	<div><select class="input-xlarge element select medium" id="element_<?php echo $count; ?>" name="<?php echo $name; ?>">
<?php
	foreach($choicelist as $key=>$val){
		if (is_array($val)){
			//$key=$val[0];
			$val=$val[$ax];
		}
		echo "<option value=\"".$key."\" ";
		if ($key==$value){ echo "selected";}
		echo ">".$val."</option>";
	}
?>
	</select>
	</div><p class="guidelines" id="guide_<?php echo $count; ?>"><small><?php echo $hint; ?></small></p>
	</li>
<?php
}
function html_Radio($count,$label,$name,$value,$radios,$hint){
?>
	<li id="li_<?php echo $count;?>" >
	<label class="description" for="element_<?php echo $count;?>"><?php echo $label;?></label>
	<div>
<?php
	$i=0;
	foreach($radios as $key=>$val){
		echo $val."<input type=\"radio\" id=\"element_{$count}_{$i}\" name=\"{$name}\" value=\"{$key}\"";
		if ($value==$key) {echo "checked";}
		echo "/>";
		$i++;
	}
?>
	</div><p class="guidelines" id="guide_<?php echo $count;?>"><small><?php echo $hint;?></small></p>
	</li>
<?php
}
function html_H3($header,$description=""){
	echo "<li class=\"section_break\"><h3>".$header."</h3>";
	if($description!=""){
	echo "<p>".$description."</p>";
	}
	echo "</li>";
}
function html_H2($header,$description=""){
	echo "<h2>".$header."</h2>";
	if($description!=""){
	echo "<p>".$description."</p>";
	}
}
function html_break(){
	echo "<li class=\"section_break\"></li>";
}

// ---------- MAIN menu pages ----
function showContent($cDB, $Pid){
	if (isset($_SESSION["authType"]) && $_SESSION["authType"]==$this->env['sessSecret']) {
		$this->login=$_SESSION["authType"];
	} else {
		$this->login=0;
	}
	$this->dbds = $cDB;
	$this->pid= $Pid;
?>
<form id="form_setup" name="main_form" action="seaded.php" class="appnitro" method="post">
<?php
	if ($this->login==0) {
		$this->print_Login("");
	} else {
		// menu
		if ($this->login==$this->env['sessSecret']){
			$viewPage="MainList";

			if (isset($_REQUEST['vpage'])) { $viewPage=$_REQUEST['vpage'];}
			switch ($viewPage){
			case "MainList":
				// radade nimekiri
				$this->MainList();
				break;
			case "infoView":
				// üks rada, list
				$viewPage=0;
				if (isset($_REQUEST['place'])) { $viewPage=$_REQUEST['place'];}
				$viewPage=(int)$viewPage;
				$this->editInfo($viewPage);
				break;
			case "trackView":
				$viewPage=0;
				if (isset($_REQUEST['place'])) { $viewPage=$_REQUEST['place'];}
				$viewPage=(int)$viewPage;
				$trackId=0;
				if (isset($_REQUEST['track'])) { $trackId=$_REQUEST['track'];}
				$trackId=(int)$trackId;
				$this->editTrack($viewPage,$trackId);
				break;
			}
		}
		}
echo "</form>";
}

//*************************************************************************************
//********************* Page views ****************************************************
//*************************************************************************************
function editTrack($placeId, $trackId) {
	echo "<a href='seaded.php?vpage=infoView&place={$placeId}'>Tagasi</a>";
	$this->html_H2("Raja andmed:");

	$sql="SELECT * FROM {$this->env['table']}track WHERE id={$trackId}";
	$rs=$this->dbds->ExecuteReader($sql);
	if ($row = @mysql_fetch_array($rs, MYSQL_ASSOC)){
		echo "<ul>";
		$this->html_InputText(1,"Nimi","track_name",$row["track_name"],"",75);
		$this->html_Choice(2,"Värv","colour",$row["colour"],"Määra raja värv (võimalusel sama, mis on kasutusel looduses)",$GLOBALS['trackColors']);
		$this->html_InputTextArea(3,"Raja GPS koordinaadid","track_data",$row["track_data"],"GPS träki failist sisu",45);
		$this->html_submitButton("Salvesta muudatused");
		$sql="SELECT * FROM {$this->env['table']}trackicons WHERE track_id={$trackId} ORDER BY typename";
		$rs2=$this->dbds->ExecuteReader($sql);
			echo "<li><ul class=\"iconnav\">";
			$i;
			while ($row2 = @mysql_fetch_array($rs2, MYSQL_ASSOC)) {
				echo html_icon($i++,$row2['typename'],"","","NOLI")."<a onclick=\"return confDelete()\" href=\"seaded.php?vpage=trackView&act=deleteTrcIc&track={$trackId}&place={$placeId}&del={$row2['id']}\">[-]</a></li>";
			}
		echo "</ul></li><li>LISA Ikoon";
		$this->html_Choice(21,"Ikoon","typename",$row2['typename'],"Vali ikoon",$GLOBALS['iconArray']);
		$this->html_submitButton("Lisa ikoon");
		echo "</li></ul>";
		$this->html_Hidden(array("form_id"=>"trackView","vpage"=>"trackView","place"=>$placeId,"track"=>$trackId));
	}
}

function editInfo($placeId) {
	echo "<a href='seaded.php'>Tagasi</a>";
	$this->html_H2("Terviseraja andmed:");

	$sql="SELECT * FROM {$this->env['table']}maininfo WHERE id={$placeId}";
	$rs=$this->dbds->ExecuteReader($sql);
	if ($row = @mysql_fetch_array($rs, MYSQL_ASSOC)){
		echo "<ul>";
		$this->html_InputText(1,"Nimetus *","name",$row['name'],"Keskuse nimi",100);
		$this->html_Choice(2,"Maakond *","county",$row['county'],"Maakond",$GLOBALS['countyArray'],0);
		$this->html_InputGeo(3,"WGS koordinaat *","geo_pos",$row['geo_pos'],"Asukoht formaadis: 56.223,23.222");
		$this->html_InputTextArea(4,"Aadress","address",$row['address'],"Keskuse aadress",4);
		$this->html_InputTextArea(5,"Kontakt","contact",$row['contact'],"Kontakt isik, telefon jms",4);
		$this->html_InputTextArea(6,"Kirjeldus","description",$row['description'],"Lühike kirjeldus",5);
		$this->html_InputText(7,"Avatud","opentime",$row['opentime'],"Keskuse lahtioleku aeg",50);
		$this->html_InputText(8,"E-post","link_email",$row['link_email'],"E-posti aadress",150);
		$this->html_InputText(9,"Koduleht","link_web",$row['link_web'],"Kodulehe aadress koos http://",150);
		$this->html_InputText(10,"Ilma aadress","link_ilmee",$row['link_ilmee'],"Asukohapõhine ilma viide",150);
		$this->html_InputText(11,"Lumeinfo ID","link_lumeinfo",$row['link_lumeinfo'],"Lumeinfo viide ID number!!",5);
		$this->html_submitButton("Salvesta muudatused");
		echo "<li><ul class=\"iconnav\">";
		$sql="SELECT * FROM {$this->env['table']}mainicons WHERE main_id={$placeId} ORDER BY groupline";
		$rs2=$this->dbds->ExecuteReader($sql);
			$rida=1;
			$i;
			while ($row2 = @mysql_fetch_array($rs2, MYSQL_ASSOC)) {
				echo html_icon($i++,$row2['typename'],$row2['url'],$row2['descript'],"NOLI")."<a onclick=\"return confDelete()\" href=\"seaded.php?vpage=infoView&act=deleteIc&place={$row['id']}&del={$row2['id']}\">[-]</a></li>";
			}
		echo "</ul></li>";
		$this->html_H2("LISA ikoonid");
		$this->html_Choice(21,"Ikoon","typename",$row2['typename'],"Vali ikoon",$GLOBALS['iconArray']);
		$this->html_InputText(22,"Rida","groupline",$row2['groupline'],"Millisel real",1);
		$this->html_InputText(23,"Viide","url",$row2['url'],"Kui on otse viide täpsustusele",150);
		$this->html_InputText(24,"Kirjeldus","descript",$row2['descript'],"Täpsustav kirjeldus",150);
		$this->html_submitButton("Lisa ikoon");
		$this->html_H2("Radade lisamine ja muutmine:");
		echo "<ul>";
		$sql="SELECT id, track_name, colour, track_data FROM {$this->env['table']}track WHERE main_id={$placeId} ORDER BY track_name";
		$rs3=$this->dbds->ExecuteReader($sql);
		while ($row3 = @mysql_fetch_array($rs3, MYSQL_ASSOC)) {
			echo "<li class=\"selurl\"><span style=\"color:{$row3['colour']};\">****</span><a href=\"seaded.php?vpage=trackView&place={$row['id']}&track={$row3['id']}\">{$row3['track_name']}</a> <a onclick=\"return confDelete()\" href=\"seaded.php?vpage=infoView&act=deleteTrck&del={$row3['id']}&place={$row['id']}\"><span class='label label-warning kustuta'> Kustuta </span></a></li>";
		}
		echo "<li> LISA uus rada";
		$this->html_InputText(31,"Nimi","track_name","","",75);
		$this->html_Choice(32,"Värv","colour","","Määra raja värv (võimalusel sama, mis on kasutusel looduses)",$GLOBALS['trackColors']);
		$this->html_InputTextArea(33,"Raja GPS koordinaadid","track_data","","GPS träki failist sisu",25);
		$this->html_submitButton("Lisa rada");
		echo "</ul>";
		$this->html_Hidden(array("form_id"=>"infoView","vpage"=>"infoView","place"=>$placeId));
	}

}

function MainList() {
	$this->html_H2("Terviserajad");
	echo "<div class='tab-content'><ol>";
	$sql="SELECT id, name, county, geo_pos FROM {$this->env['table']}maininfo ORDER BY name";
	$rs=$this->dbds->ExecuteReader($sql);
	while ($row = @mysql_fetch_array($rs, MYSQL_ASSOC)) {
		echo "<li class=\"selurl\"><a style=\"width: 350px;\" href=\"seaded.php?vpage=infoView&place={$row['id']}\">{$row['name']} | <span class='maakond'>{$row['county']}</span></a>  <a onclick=\"return confDelete()\" href=\"seaded.php?vpage=MainList&act=delete&place={$row['id']}\"><span class='label label-warning kustuta'>Kustuta</span></a></li>";
	}
	echo "</ol></div>";
	// $this->html_break();
	$this->html_H2("Lisa uus");
	echo "<div class='tab-content'><ul>";
	$this->html_Choice(1,"Maakond","county","","Vali maakond",$GLOBALS['countyArray'],0);
	$this->html_InputText(2,"Nimetus","name","","Sisesta Terviseraja nimetus",100);
	$this->html_submitButton("Lisa uus terviserada");
	echo "</ul></div>";
	$this->html_Hidden(array("form_id"=>"MainList","vpage"=>"MainList"));
}

function print_Login($id){ ?>
<label class="description" for="element_3">Log In</label>
	<div>
			<label class="description" for="element_1">Kasutaja/e-post</label><div>
				<input id="element_2" class="element text medium" maxlength="60" name="email" type="text" value="" />
			</div>
			<label class="description" for="element_2">Parool</label><div>
				<input id="element_3" class="element text medium" maxlength="25" name="passw" type="password" value="" />
			</div>
			<input id="saveForm" class="button_text" name="submitAPass" type="submit" value="Log In" />
	</div>
<?php
} }
?>