var map = null;
var color = new Array();
var trck = new Array();
var posCenter = null;
var posElementDoc = null;

function confDelete() {
	var answer = confirm("Soovid kustutada?");
	if (answer){
		return true;
	}
	else{
		return false;
	}
}
function changeVisible(elem_id)
{
	if (document.getElementById(elem_id).style.display=='none') {
		document.getElementById(elem_id).style.display='block';
	} else {document.getElementById(elem_id).style.display='none';}
}

function ShowLocation(mapPos)
{
	if (map!=null){
		var n=mapPos.split(",");
		map.setView({ zoom: 11, center: new Microsoft.Maps.Location(n[0],n[1]) })
	}
}
function ShowTrack(id)
{
	map.entities.clear();
	if (trck[id]!=null){
		var objStart = trck[id][0];
		var linestyle = { strokeColor: new Microsoft.Maps.Color.fromHex(color[id]), strokeThickness: 3};
		var pushpin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(objStart.a, objStart.o), pushpinStart);
		map.entities.push(pushpin);
		var polArray = new Array();
		var kmPost = 1000;
		var maxH=0; var minH=999;
		for(var i=0; i<trck[id].length; i++) {
			var objPoint = trck[id][i];
			var polPoint = new Microsoft.Maps.Location(objPoint.a, objPoint.o);
            polArray.push(polPoint);
			if (objPoint.d>kmPost) {
				pushpinKMstyle.text=(kmPost/1000).toFixed(0);
				var pushpinKM = new Microsoft.Maps.Pushpin(polPoint, pushpinKMstyle);
				map.entities.push(pushpinKM);
				//var infopin = new Microsoft.Maps.Infobox(polPoint, { title: (kmPost/1000).toFixed(0)+' km', description: 'Kõrgus:'+objPoint.e+' m', pushpin: pushpinKM});
				//map.entities.push(infopin);
				kmPost=kmPost+1000;
			}
			if (objPoint.e>maxH) { maxH=objPoint.e;}
			if (objPoint.e<minH) { minH=objPoint.e;}
		}
		var polyline = new Microsoft.Maps.Polyline(polArray, linestyle);
		map.entities.push(polyline);
		map.setView({ zoom: 13, center: new Microsoft.Maps.Location(objStart.a, objStart.o) })
	}
}
function GetBingMap(mapAreaName, posElement) {
	posElementDoc = document.getElementById(posElement);
	var pos= posElementDoc.value.toString();
	if (pos != "") {
		var posLL = pos.split(",", 2);
		posCenter = new Microsoft.Maps.Location(posLL[0], posLL[1]);
	} else {
		posCenter = new Microsoft.Maps.Location(58.5970, 24.9280);
	}
	map = new Microsoft.Maps.Map(document.getElementById(mapAreaName),
		{ credentials: "Akc3ga1mT4PNKTWoXk7HTYOb-FsJ5tNPhOTF2Y9iep_TLRz0lxcTk4XpS10GWlaz",
			showScalebar: true,
			showDashboard: true,
			enableSearchLogo: false,
			mapTypeId: Microsoft.Maps.MapTypeId.road,
			zoom: 5,
			center: posCenter
		});
		Microsoft.Maps.Events.addHandler(map, 'click', getLatLong);

	setPin();
}
function getLatLong(e) {
	if (e.targetType == "map") {
		var point = new Microsoft.Maps.Point(e.getX(), e.getY());
		var loc = e.target.tryPixelToLocation(point);
		var sLat = loc.latitude.toString();
		var sLon = loc.longitude.toString();
		if (posElementDoc!=null){
			posElementDoc.value = sLat.substring(0,8)+','+sLon.substring(0,8);
			posCenter = loc;
			setPin();
		}
	}
}
function setPin() {
	var pushPin = new Microsoft.Maps.Pushpin(posCenter,
	 { text: 'Keskus', visible: true });
	map.entities.clear();
	map.entities.push(pushPin);
	map.setView({ center: posCenter })
}
function countyCenter(x,y,z){
	var maph=document.getElementById('etrMap');
	if (map!=null && maph!=null){
		map.setView({ zoom: z, center: new Microsoft.Maps.Location(x,y) })
	}
}