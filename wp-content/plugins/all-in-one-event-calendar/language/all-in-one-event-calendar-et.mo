��    Z      �     �      �     �     �      �  &   �          %     <  
   E     P     ^     o     x  
   �     �     �     �     �      �     �     		     	      	     %	     +	  &   H	     o	     s	     	     �	     �	     �	  *   �	  3   �	     
  
   #
     .
     B
     R
     i
  (   �
  *   �
     �
     �
     �
  
   �
               $     2     8     =     L  $   O     t     �  	   �     �     �     �     �  	   �  	   �     �     �            2   3  V   f     �     �     �     �     �                         '     3     R  
   W     b     j     p     w     {     �     �     �  �  �     \     e     v  '   �     �     �     �     �               -     6  
   D     O     \     o     �  0   �     �     �     �     �     �  	               	   #  "   -     P     X     j     }     �     �     �     �     �     �          -  1   A  	   s     }     �     �     �     �     �     �     �     �       /   
     :     J  
   O     Z     a     h     o     �     �     �     �     �     �  .   �  G   -     u  
   �     �     �     �     �     �     �     �     �  "        %  	   2     <     J     R     W  &   [     �     �  	   �             1      ?                    *   #   %   T       9             >             +   W       Y           "      (      )   
   O   !   @       E   	   =   .          K          4           V   Z   Q   B      <   A   R   8              G           N       U      2       6          ;   F                  ,   I   7       0       S   5   3         :               P   D       -              '   /   X      H   $                     C             M                L   &   J    Add New Add New Event Add a new event to the calendar. Add this event to your Google Calendar Add to Google Add to Google Calendar Address: All Events All-day event Back to Calendar Calendar Calendar Feeds Categories Categories: Category Color Choose Your Theme Choose a rule for exclusion Choose specific dates to exclude Collapse All Contact name: Contact: Cost Cost: Custom post type nameEvents Custom post type name (singular)Event Day Description Donation Based E-mail: End date / time Event Details Event categories taxonomyEvent Categories Event categories taxonomy (singular)Event Category Event category: Event cost Event date and time Event date/time Event location details Event tags taxonomyEvent Tags Event tags taxonomy (singular)Event Tag Event updated. <a href="%s">View event</a> Events Exclude Exclude dates Expand All Filter: Manage Event Categories Manage Events Month Name Next Events » No Organize and color-code your events. Organizer contact info Phone: Post Date Post Your Event Publish Repeat Select date range Set Price Show All  Show Google Map: Start date / time Submit for Review Subscribe in Google Calendar Subscribe to this calendar in your Google Calendar Subscribe to this calendar using your favourite calendar program (iCal, Outlook, etc.) Successfully imported events: Summary: Tags Tags: There are no upcoming events. Today Type Update Upgrade Venue name: View and edit all your events. Week Week of %s Welcome When: Where: Yes to this filtered calendar « Previous Events ✔ Add to Calendar ✔ Subscribe Project-Id-Version: All-in-One Event Calendar by Timely 1.8 Premium
Report-Msgid-Bugs-To: http://wordpress.org/tag/all-in-one-event-calendar
POT-Creation-Date: 2012-07-25 00:36:17+00:00
PO-Revision-Date: 2013-01-15 18:27+0200
Last-Translator: kuulind <info@kuulind.eu>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
 Lisa Uus Lisa Uus Üritus Lisa uus üritus kalendrisse Lisa see üritus oma Google kalendrisse Lisa Google–sse Lisa oma Google kalendrisse Aadress: Kõik Üritused Kogu-päeva üritus Tagasi kalendri juurde Kalender Kalendri vood Kategooria Kategooriad: Kategooria värvus Vali teema/vormindus Vali välja arvamise reegel Määra konkreetsed kuupäevad välja jätmiseks Sulge laivaade Kontaktisik: Kontaktisik: Hind Hind: Üritused Üritus Päevavaade Kirjeldus Annetuste põhine/mittetulunduslik E-post: Lõpp kuu / päev Ürituse Kirjeldus Ürituste Kategooriad Ürituse Kategooria Ürituse kategooria: Ürituse Maksumus Ürituse Kuupäev ja Aeg Ürituse Kuupäev/aeg Ürituse asukohaga seonduv Ürituse Märksõnad Ürituse Märksõna Üritus muudetud. <a href="%s">Vaata üritust</a> Üritused Välja arvatud Kuupäevad mis jäävad välja Ava laivaade Filter: Halda Ürituste kategooriad Halda üritusi Kuuvaade Nimi Järgmised » Ei Korrasta ja määra värvid oma ürituste jaoks Korraldaja info Tel: Postitatud Avalda Avalda Kordub Määra kuupäevade vahemik Määra hind Näita Kõiki Näita Google Map Algus kuu / päev Esita ülevaatamiseks Ühilda oma Google kalendriga Liida see kalender oma Google konto kalendriga Ühilda see kalender oma lemmik kalendertarkvaraga (iCal, Outlook jms). Edukalt imporditud: Kirjeldus: Märksõnad Märksõnad: Eelseisvaid sündmusi ei ole. Täna Tüüp Uuenda Uuenda Toimumiskoht/spordiväljak: Vaata ja halda kõiki oma üritusi Nädalavaade Nädal %s Tere tulemast Millal: Kus: Jah see kohandatud kalender oma kalendriga « Eelmised ✔ Lisa kalendrisse ✔ Liitu 